FROM nginx
RUN apt update -y && apt install unzip -y && apt install wget -y
RUN mkdir -p /var/www/html
RUN cd /var/www/html && wget https://www.free-css.com/assets/files/free-css-templates/download/page291/drool.zip
RUN cd /var/www/html && unzip drool.zip
COPY ./conf.d/drool.conf /etc/nginx/conf.d
EXPOSE 8080